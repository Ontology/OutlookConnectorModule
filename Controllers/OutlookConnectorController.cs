﻿using ElasticSearchNestConnector;
using OntologyAppDBConnector;
using OntologyClasses.BaseClasses;
using OntoMsg_Module;
using OntoMsg_Module.Attributes;
using OntoMsg_Module.Base;
using OntoMsg_Module.Models;
using OntoMsg_Module.Notifications;
using OntoMsg_Module.StateMachines;
using OntoMsg_Module.WebSocketServices;
using OutlookConnectorModule.Services;
using SecurityModule;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;

namespace OutlookConnectorModule.Controllers
{
    public class OutlookConnectorController : OutlookConnectorViewModel, IViewController
    {
        private WebsocketServiceAgent webSocketServiceAgent;

        private string userName;
        private string password;
        private string group;

        private clsLocalConfig localConfig;

        private SecurityController securityController;

        private clsDataWork_OutlookItems dataWorkOutlookItems;

        private clsOntologyItem oItemUser;
        private clsOntologyItem oItemGroup;

        public IControllerStateMachine StateMachine { get; private set; }
        public ControllerStateMachine LocalStateMachine
        {
            get
            {
                if (StateMachine == null)
                {
                    return null;
                }

                return (ControllerStateMachine)StateMachine;
            }
        }

        public OutlookConnectorController()
        {
            // local configuration
            localConfig = (clsLocalConfig)LocalConfigManager.GetLocalConfig(((GuidAttribute)Assembly.GetExecutingAssembly().GetCustomAttributes(true).FirstOrDefault(objAttribute => objAttribute is GuidAttribute)).Value);
            if (localConfig == null)
            {
                localConfig = new clsLocalConfig(new Globals());
                LocalConfigManager.AddLocalConfig(localConfig);
            }

            Initialize();
        }

        private void Initialize()
        {
            var stateMachine = new ControllerStateMachine(StateMachineType.BlockSelectingSelected | StateMachineType.BlockLoadingSelected);
            stateMachine.IsControllerListen = true;
            StateMachine = stateMachine;
            stateMachine.PropertyChanged += StateMachine_PropertyChanged;
            stateMachine.loadSelectedItem += StateMachine_loadSelectedItem;
            stateMachine.loginSucceded += StateMachine_loginSucceded;
            stateMachine.openedSocket += StateMachine_openedSocket;
            stateMachine.closedSocket += StateMachine_closedSocket;

            securityController = new SecurityController(localConfig.Globals);
            PropertyChanged += OutlookConnectorController_PropertyChanged;
        }

        private void StateMachine_closedSocket()
        {
            
        }

        private void StateMachine_openedSocket()
        {
            webSocketServiceAgent.RegisterEndpoint(new ChannelEndpoint
            {
                EndPointId = webSocketServiceAgent.EndpointId,
                EndpointType = EndpointType.Sender,
                SessionId = webSocketServiceAgent.DataText_SessionId
            });
        }

        private void StateMachine_loginSucceded()
        {
            throw new NotImplementedException();
        }

        private void StateMachine_loadSelectedItem(clsOntologyItem oItemSelected)
        {
            
        }

        private void StateMachine_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            
        }

        private void OutlookConnectorController_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            var property = ViewModelProperties.FirstOrDefault(viewProperty => viewProperty.Property.Name == e.PropertyName);

            if (property == null) return;

            property.ViewItem.AddValue(property.Property.GetValue(this));

            webSocketServiceAgent.SendPropertyChange(e.PropertyName);
        }

        public void InitializeViewController(WebsocketServiceAgent webSocketServiceAgent)
        {
            this.webSocketServiceAgent = webSocketServiceAgent;
            this.webSocketServiceAgent.PropertyChanged += WebSocketServiceAgent_PropertyChanged;
            this.webSocketServiceAgent.comServerOnMessage += WebSocketServiceAgent_comServerOnMessage;
            this.webSocketServiceAgent.comServerOpened += WebSocketServiceAgent_comServerOpened;


        }

        
        private void WebSocketServiceAgent_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName == OntoMsg_Module.Notifications.NotifyChanges.Websocket_DataText_SessionId)
            {

           
            }
            else if (e.PropertyName == OntoMsg_Module.Notifications.NotifyChanges.Websocket_Command_RequestedCommand)
            {



            }
            else if (e.PropertyName == OntoMsg_Module.Notifications.NotifyChanges.Websocket_Event_RequestEvent)
            {

            }
            else if (e.PropertyName == OntoMsg_Module.Notifications.NotifyChanges.Websocket_ChangedProperty)
            {
                if (webSocketServiceAgent.ChangedProperty.Key.ToLower() == "username")
                {
                    userName = webSocketServiceAgent.ChangedProperty.Value.ToString();
                    CheckLogin();
                }
                if (webSocketServiceAgent.ChangedProperty.Key.ToLower() == "password")
                {
                    password = webSocketServiceAgent.ChangedProperty.Value.ToString();
                    CheckLogin();
                }
                if (webSocketServiceAgent.ChangedProperty.Key.ToLower() == "group")
                {
                    group = webSocketServiceAgent.ChangedProperty.Value.ToString();
                    CheckLogin();
                }
                
            }
            else if (e.PropertyName == NotifyChanges.Websocket_ChangedViewItems)
            {
                if (!IsSuccessful_Login) return;
                webSocketServiceAgent.ChangedViewItems.ForEach(changedItem =>
                {
                    if (changedItem.ViewItemId == Notifications.NotifyChanges.Navigation_MailDocs)
                    {
                        var mailDocs = Newtonsoft.Json.JsonConvert.DeserializeObject<List<clsAppDocuments>>(changedItem.ViewItemValue.ToString());
                        var result = dataWorkOutlookItems.SyncDocuments(mailDocs);
                    }
                });
            }


        }

        private void CheckLogin()
        {
            if (!string.IsNullOrEmpty(userName) && !string.IsNullOrEmpty(group) && !string.IsNullOrEmpty(password))
            {
                oItemUser = securityController.GetUser(userName);
                oItemGroup = securityController.GetGroup(group);

                if (oItemUser == null || oItemGroup == null)
                {

                    IsSuccessful_Login = false;
                }

                var result = securityController.InitializeUser(oItemUser, password);
                if (result.GUID == localConfig.Globals.LState_Success.GUID)
                {
                    dataWorkOutlookItems = new clsDataWork_OutlookItems(localConfig.Globals, oItemUser);
                    IsSuccessful_Login = true;
                }
                else
                {
                    IsSuccessful_Login = false;
                }
            }
        }

        private void WebSocketServiceAgent_comServerOpened()
        {
           

        }

        private void WebSocketServiceAgent_comServerOnMessage(OntoMsg_Module.Notifications.InterServiceMessage message)
        {
            if (message.ReceiverId != null && message.ReceiverId != webSocketServiceAgent.EndpointId) return;
            
        }

        public List<ViewModelProperty> GetViewModelProperties(bool onlySend = true, ViewItemType viewItemType = ViewItemType.All, ViewItemClass viewItemClass = ViewItemClass.All)
        {
            return ViewModelProperties.Where(viewItemProp => (onlySend ? viewItemProp.ViewModelAttribute.Send : 1 == 1)
                && (viewItemType != ViewItemType.All ? viewItemProp.ViewModelAttribute.ViewItemType == viewItemType : 1 == 1)
                && (viewItemClass != ViewItemClass.All ? viewItemProp.ViewModelAttribute.ViewItemClass == viewItemClass : 1 == 1)).ToList();
        }
    }
}

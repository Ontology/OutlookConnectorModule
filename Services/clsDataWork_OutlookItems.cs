﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using OntologyAppDBConnector;
using OntologyClasses.BaseClasses;
using OntoMsg_Module;
using System.Runtime.InteropServices;
using OutlookConnectorModule.Models;
using ElasticSearchNestConnector;

namespace OutlookConnectorModule.Services
{
    public class clsDataWork_OutlookItems
    {
        private clsLocalConfig objLocalConfig;

        private OntologyModDBConnector objDBLevel_OutlookItems_Att;
        private OntologyModDBConnector objDBLevel_OutlookItems_Rel1;
        private OntologyModDBConnector objDBLevel_OutlookItems_Rel2;
        private OntologyModDBConnector objDBLevel_EmailAddress;
        private OntologyModDBConnector objDBLevel_EntryID;

        private OntologyModDBConnector objDBLevel_RefToMailItem;

        private clsUserAppDBSelector objAppDBLevel;
        private clsUserAppDBUpdater dbUpdater;

        public clsOntologyItem OItem_Result_OutlookItems { get; private set; }
        public clsOntologyItem OItem_Result_Documents { get; private set; }

        public List<clsObjectRel> OList_OutlookItems
        {
            get { return objDBLevel_OutlookItems_Rel2.ObjectRels; }
        }

        private clsDataWork_OutlookConnector objDataWork_OutlookConnector;

        public List<clsObjectRel> OList_MailItem_LeftRight
        {
            get { return objDBLevel_OutlookItems_Rel1.ObjectRels; }
        }

        public List<clsMailItem> MailItems = new List<clsMailItem>();

        public clsOntologyItem GetOItemByEntryID(string strEntryID)
        {
            var objOList_OItems = objDBLevel_OutlookItems_Rel2.ObjectRels.Where(p => p.Name_Object == strEntryID).Select(p => new clsOntologyItem {GUID = p.ID_Other,
                Name = p.Name_Other,
                GUID_Parent = p.ID_Parent_Other,
                Type = objLocalConfig.Globals.Type_Object}).ToList();
            if (objOList_OItems.Any())
            {
                return objOList_OItems.First();
            }
            else
            {
                return null;
            }
        }

        public string GetEntryID(clsOntologyItem oItem_Email)
        {

            var searchOutlookItem = new List<clsObjectRel>{ new clsObjectRel {ID_Other = oItem_Email.GUID,
                    ID_RelationType = objLocalConfig.OItem_relationtype_is.GUID,
                    ID_Parent_Object = objLocalConfig.OItem_type_outlook_item.GUID } };

            var result = objDBLevel_EntryID.GetDataObjectRel(searchOutlookItem, doIds: false);

            if (result.GUID == objLocalConfig.Globals.LState_Success.GUID)
            {
                if (objDBLevel_EntryID.ObjectRels.Any())
                {
                    return objDBLevel_EntryID.ObjectRels.First().Name_Object;
                }
                else
                {
                    return "";
                }
            }
            else
            {
                return null;
            }
        }

        public List<clsMailItem> GetData_EmailItemByRefOfOItem(clsOntologyItem OITem_Ref, clsOntologyItem OItem_RelationType, clsOntologyItem OItem_Direction)
        {
            var mailItems = new List<clsMailItem>();

            List<clsObjectRel> oRel_Ref_To_mailItems;

            if (OItem_Direction.GUID == objLocalConfig.Globals.Direction_LeftRight.GUID)
            {
                oRel_Ref_To_mailItems = new List<clsObjectRel> 
                {
                    new clsObjectRel 
                    {
                        ID_Object = OITem_Ref.GUID,
                        ID_RelationType = OItem_RelationType.GUID,
                        ID_Parent_Other = objLocalConfig.OItem_type_e_mail.GUID 
                    } 
                };

            }
            else
            {
                oRel_Ref_To_mailItems = new List<clsObjectRel> 
                {
                    new clsObjectRel 
                    {
                        ID_Other = OITem_Ref.GUID,
                        ID_RelationType = OItem_RelationType.GUID,
                        ID_Parent_Object = objLocalConfig.OItem_type_e_mail.GUID 
                    } 
                };
            }

            var objOItem_Result = objDBLevel_RefToMailItem.GetDataObjectRel(oRel_Ref_To_mailItems, doIds: false);

            if (objOItem_Result.GUID == objLocalConfig.Globals.LState_Success.GUID)
            {
                if (objDBLevel_RefToMailItem.ObjectRels.Any())
                {
                    var objOList_MailItems = objDBLevel_RefToMailItem.ObjectRels.Select(mi => new clsOntologyItem
                    {
                        GUID = OItem_Direction.GUID == objLocalConfig.Globals.Direction_LeftRight.GUID ? mi.ID_Other : mi.ID_Object,
                        Name = OItem_Direction.GUID == objLocalConfig.Globals.Direction_LeftRight.GUID ? mi.Name_Object : mi.Name_Other,
                        GUID_Parent = objLocalConfig.OItem_type_e_mail.GUID,
                        Type = objLocalConfig.Globals.Type_Object
                    }).ToList();

                    GetData_OutlookItems(objOList_MailItems);

                    if (OItem_Result_OutlookItems.GUID == objLocalConfig.Globals.LState_Success.GUID)
                    {
                        mailItems = (from objMailItem in objOList_MailItems
                                     join objSended in objDBLevel_OutlookItems_Att.ObjAtts on objMailItem.GUID equals objSended.ID_Object
                                     join objVon in objDBLevel_OutlookItems_Rel1.ObjectRels.Where(oi => oi.ID_RelationType == objLocalConfig.OItem_relationtype_von.GUID).ToList() on
                                        objMailItem.GUID equals objVon.ID_Object
                                     join objAn in objDBLevel_OutlookItems_Rel1.ObjectRels.Where(oi => oi.ID_RelationType == objLocalConfig.OItem_relationtype_an.GUID).ToList() on
                                        objMailItem.GUID equals objAn.ID_Object
                                     select new clsMailItem
                                     {
                                         ID_OItem = objMailItem.GUID,
                                         Name_OItem = objMailItem.Name,
                                         CreationDate = (DateTime)objSended.Val_Date,
                                         SenderEmail = objVon.Name_Other,
                                         To = objAn.Name_Other
                                     }).ToList();
                    }
                    else
                    {
                        mailItems = null;
                    }
                }
            }
            else
            {
                mailItems = null;
            }

            return mailItems;
        }

        public void GetData_OutlookItems(List<clsOntologyItem> OList_MailItems = null)
        {
            OItem_Result_OutlookItems = objLocalConfig.Globals.LState_Nothing.Clone();

            List<clsObjectAtt> objOAL_Mail__Sended;
            if (OList_MailItems == null || !OList_MailItems.Any() || OList_MailItems.Count > 500)
            {
                objOAL_Mail__Sended = new List<clsObjectAtt> { new clsObjectAtt {ID_Class = objLocalConfig.OItem_type_e_mail.GUID,
                    ID_AttributeType = objLocalConfig.OItem_attribute_sended.GUID } };
            }
            else
            {
                objOAL_Mail__Sended = OList_MailItems.Select(mi => new clsObjectAtt { ID_Object = mi.GUID,
                    ID_AttributeType = objLocalConfig.OItem_attribute_sended.GUID } ).ToList();
                
                    
                
            }


            List<clsObjectRel> objORL_Mail_LeftRight;

            if (OList_MailItems == null || !OList_MailItems.Any() || OList_MailItems.Count > 500)
            {
                objORL_Mail_LeftRight = new List<clsObjectRel> { 
                    new clsObjectRel {ID_Parent_Object = objLocalConfig.OItem_type_e_mail.GUID,
                        ID_Parent_Other = objLocalConfig.OItem_type_email_address.GUID,
                        ID_RelationType = objLocalConfig.OItem_relationtype_von.GUID },
                    new clsObjectRel {ID_Parent_Object = objLocalConfig.OItem_type_e_mail.GUID,
                        ID_Parent_Other = objLocalConfig.OItem_type_email_address.GUID,
                        ID_RelationType = objLocalConfig.OItem_relationtype_an.GUID} };
            }
            else
            {
                objORL_Mail_LeftRight = OList_MailItems.Select(mi => new clsObjectRel
                {
                    ID_Object = mi.GUID,
                    ID_Parent_Other = objLocalConfig.OItem_type_email_address.GUID,
                    ID_RelationType = objLocalConfig.OItem_relationtype_von.GUID
                }).ToList();

                objORL_Mail_LeftRight.AddRange(OList_MailItems.Select(mi => new clsObjectRel
                {
                    ID_Object = mi.GUID,
                    ID_Parent_Other = objLocalConfig.OItem_type_email_address.GUID,
                    ID_RelationType = objLocalConfig.OItem_relationtype_an.GUID
                }));



            }

            List<clsObjectRel> objORL_Mail_RightLeft;

            if (OList_MailItems == null || !OList_MailItems.Any() || OList_MailItems.Count > 500)
            {
                objORL_Mail_RightLeft = new List<clsObjectRel> {
                new clsObjectRel {ID_Parent_Other = objLocalConfig.OItem_type_e_mail.GUID,
                    ID_RelationType = objLocalConfig.OItem_relationtype_is.GUID,
                    ID_Parent_Object = objLocalConfig.OItem_type_outlook_item.GUID } };
            }
            else
            {
                objORL_Mail_RightLeft = OList_MailItems.Select(mi => new clsObjectRel { ID_Other = mi.GUID,
                    ID_RelationType = objLocalConfig.OItem_relationtype_is.GUID,
                    ID_Parent_Object = objLocalConfig.OItem_type_outlook_item.GUID }).ToList();



            }

            

            var objOItem_Result = objDBLevel_OutlookItems_Att.GetDataObjectAtt(objOAL_Mail__Sended, doIds: false);
            if (objOItem_Result.GUID == objLocalConfig.Globals.LState_Success.GUID)
            {
                objOItem_Result = objDBLevel_OutlookItems_Rel1.GetDataObjectRel(objORL_Mail_LeftRight, doIds: false);
                if (objOItem_Result.GUID == objLocalConfig.Globals.LState_Success.GUID)
                {
                    objOItem_Result = objDBLevel_OutlookItems_Rel2.GetDataObjectRel(objORL_Mail_RightLeft, doIds: false);
                    if (objOItem_Result.GUID == objLocalConfig.Globals.LState_Success.GUID)
                    {

                        OItem_Result_OutlookItems = objOItem_Result;                                                
                    }
                    else
                    {
                        OItem_Result_OutlookItems = objOItem_Result;
                    }
                }
                else
                {
                    OItem_Result_OutlookItems = objOItem_Result;
                }
            }
            else
            {
                OItem_Result_OutlookItems = objOItem_Result;
            }
        }

        public clsOntologyItem SyncDocuments(List<clsAppDocuments> outlookMails)
        {
            var documents = objAppDBLevel.GetData_Documents();

            var docsToSave = (from mail in outlookMails.Where(mailItem => mailItem.Dict.ContainsKey("EntryID"))
                              join doc in documents.Where(mailItem => mailItem.Dict.ContainsKey("EntryID")) on mail.Dict["EntryID"] equals doc.Dict["EntryID"] into documentsInDb
                              from doc in documentsInDb.DefaultIfEmpty()
                              where doc == null
                              select mail).ToList();

            var result = dbUpdater.SaveDoc(docsToSave);

            return result;
                              
        }

        public void GetData_Documents(List<clsOntologyItem> OList_MailItems = null)
        {
            OItem_Result_Documents = objLocalConfig.Globals.LState_Nothing.Clone();

            GetData_OutlookItems(OList_MailItems);
            if (OItem_Result_OutlookItems.GUID == objLocalConfig.Globals.LState_Success.GUID)
            {
                List<clsAppDocuments> Documents = objAppDBLevel.GetData_Documents();

                var _MailItems = (from objDoc in Documents.ToList()
                                  join objOutlookItem in objDBLevel_OutlookItems_Rel2.ObjectRels on objDoc.Dict["EntryID"].ToString().ToLower() equals objOutlookItem.Name_Object.ToLower() into objOutlookItems
                                  from objOutlookItem in objOutlookItems.DefaultIfEmpty()
                                  select new clsMailItem 
                                  {
                                        ID_MailItem = objDoc.Id,
                                        EntryID = objDoc.Dict["EntryID"].ToString(),
                                        CreationDate = objDoc.Dict["CreationDate"] != null ? (DateTime)objDoc.Dict["CreationDate"] : new DateTime(),
                                        SenderEmail = objDoc.Dict["SenderEmailAddress"] != null ? objDoc.Dict["SenderEmailAddress"].ToString() : null,
                                        SenderName = objDoc.Dict.ContainsKey("SenderName") && objDoc.Dict["SenderName"] != null ? objDoc.Dict["SenderName"].ToString() : null,
                                        Subject = objDoc.Dict.ContainsKey("Subject") && objDoc.Dict["Subject"] != null ? objDoc.Dict["Subject"].ToString() : "",
                                        To = objDoc.Dict["To"] != null ? objDoc.Dict["To"].ToString() : null,
                                        SemItemPresent = objOutlookItem != null ? true : false,
                                        ID_OItem = objOutlookItem != null ? objOutlookItem.ID_Other : null,
                                        Name_OItem = objOutlookItem != null ? objOutlookItem.Name_Other : null
                                    }).ToList();    

                
                MailItems = new List<clsMailItem>(_MailItems);
                OItem_Result_Documents = objLocalConfig.Globals.LState_Success.Clone();
            }
            else
            {
                OItem_Result_Documents = OItem_Result_Documents.Clone();
            }
            
            
            
        }

        public clsDataWork_OutlookItems(clsLocalConfig LocalConfig, clsDataWork_OutlookConnector DataWork_OutlookConnector)
        {
            objLocalConfig = LocalConfig;
            objDataWork_OutlookConnector = DataWork_OutlookConnector;
            Initialize();
        }

        public clsDataWork_OutlookItems(Globals Globals, clsOntologyItem OItem_User)
        {
            objLocalConfig = (clsLocalConfig)LocalConfigManager.GetLocalConfig(((GuidAttribute)Assembly.GetExecutingAssembly().GetCustomAttributes(true).FirstOrDefault(objAttribute => objAttribute is GuidAttribute)).Value);
            if (objLocalConfig == null)
            {
                objLocalConfig = new clsLocalConfig(Globals);
                LocalConfigManager.AddLocalConfig(objLocalConfig);
            }

            objLocalConfig.User = OItem_User;
            objDataWork_OutlookConnector = new clsDataWork_OutlookConnector(objLocalConfig);
            Initialize();
        }

        public List<clsOntologyItem> GetData_EmailAddress(string strEmailAddress)
        {
            var objOList_Objects = new List<clsOntologyItem> { new clsOntologyItem {Name = strEmailAddress,
                GUID_Parent = objLocalConfig.OItem_type_email_address.GUID}};

            var objOList_Result = new List<clsOntologyItem>();

            var objOItem_Result =  objDBLevel_EmailAddress.GetDataObjects(objOList_Objects,exact:true);

            if (objOItem_Result.GUID == objLocalConfig.Globals.LState_Success.GUID)
            {
                objOList_Result = objDBLevel_EmailAddress.Objects1;
            }
            else
            {
                objOList_Result = null;
            }

            return objOList_Result;

        }
        private void Initialize()
        {

            objDBLevel_OutlookItems_Att = new OntologyModDBConnector(objLocalConfig.Globals);
            objDBLevel_OutlookItems_Rel1 = new OntologyModDBConnector(objLocalConfig.Globals);
            objDBLevel_OutlookItems_Rel2 = new OntologyModDBConnector(objLocalConfig.Globals);
            objDBLevel_EmailAddress = new OntologyModDBConnector(objLocalConfig.Globals);
            objDBLevel_RefToMailItem = new OntologyModDBConnector(objLocalConfig.Globals);
            objDBLevel_EntryID = new OntologyModDBConnector(objLocalConfig.Globals);

            objAppDBLevel = new ElasticSearchNestConnector.clsUserAppDBSelector(objLocalConfig.Globals.Server, objLocalConfig.Globals.Port,objDataWork_OutlookConnector.Ontology.GUID, objLocalConfig.User.GUID, objLocalConfig.Globals.SearchRange, objLocalConfig.Globals.Session);
            dbUpdater = new clsUserAppDBUpdater(objAppDBLevel);

            OItem_Result_OutlookItems = objLocalConfig.Globals.LState_Nothing.Clone();
            OItem_Result_Documents = objLocalConfig.Globals.LState_Nothing.Clone();
        }

        
    }
}

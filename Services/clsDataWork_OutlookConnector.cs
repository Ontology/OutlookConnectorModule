﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OntologyAppDBConnector;
using OntologyClasses.BaseClasses;

namespace OutlookConnectorModule.Services
{
    public class clsDataWork_OutlookConnector
    {
        private clsLocalConfig objLocalConfig;

        private OntologyModDBConnector objDBLevel_Ontology;
        

        public clsOntologyItem Ontology { get; private set; }

        

        

        public clsDataWork_OutlookConnector(clsLocalConfig LocalConfig)
        {
            objLocalConfig = LocalConfig;
            Initialize();
        }

        private void GetData_Ontology()
        {
            var objOList_Ontology = new List<clsOntologyItem> { new clsOntologyItem { GUID = objLocalConfig.ID_Ontology } };

            var objOItem_Result = objDBLevel_Ontology.GetDataObjects(objOList_Ontology);
            if (objOItem_Result.GUID == objLocalConfig.Globals.LState_Success.GUID)
            {
                if (objDBLevel_Ontology.Objects1.Any())
                {
                    Ontology = objDBLevel_Ontology.Objects1.First();
                }
                else
                {
                    throw new Exception("Die notwendigen Daten konnten nicht ermittelt werden!");
                }
            }
            else
            {
                throw new Exception("Die notwendigen Daten konnten nicht ermittelt werden!");
            }
        }

        

        private void Initialize()
        {
            objDBLevel_Ontology = new OntologyModDBConnector(objLocalConfig.Globals);
            GetData_Ontology();
        }
    }

}
